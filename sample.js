var iniIcadata;
var sessionId;
var sessionObj;
var appIds = [];

//Default for vars not loaded from cPanel
var loginDomain = "example.com";
var userUsername = "";
var userPassword = "";
var icaAddress = "";
var icaInitialProgram = "";
var icaTitle = "";


document.addEventListener("DOMContentLoaded",function()
{
  chrome.storage.managed.get(null,function(result)
  {
    if (result!==undefined && result["loginDomain"]!==undefined)
    {
      loginDomain = result["loginDomain"];
      console.log("loginDomain from JSON: " + loginDomain);
    }
    
    if (result!==undefined && result["userUsername"]!==undefined)
    {
      userUsername = result["userUsername"];
      console.log("userUsername from JSON: " + userUsername);
    }
    
    if (result!==undefined && result["userPassword"]!==undefined)
    {
      userPassword = result["userPassword"];
      console.log("userPassword from JSON: <hidden>");
    }
    
    if (result!==undefined && result["icaAddress"]!==undefined)
    {
      icaAddress = result["icaAddress"];
      console.log("icaAddress from JSON: " + icaAddress);
    }
    
    if (result!==undefined && result["icaInitialProgram"]!==undefined)
    {
      icaInitialProgram = result["icaInitialProgram"];
      console.log("icaInitialProgram from JSON: " + icaInitialProgram);
    }
    
    if (result!==undefined && result["icaTitle"]!==undefined)
    {
      icaTitle = result["icaTitle"];
      console.log("icaTitle from JSON: " + icaTitle);
    }
    
  });

	init();
});


//Use appropriate citrix receiver id. This sample uses EAR.
// EAR = lbfgjakkeeccemhonnolnmglmfmccaag , prod version = haiffjcadagjlijoggckpgfnoeiflnem
//kdndmepchimlohdcdkokdddpbnniijoa
var citrixReceiverId = "haiffjcadagjlijoggckpgfnoeiflnem";


function init()
{
  var sessionObj;
	document.getElementById("createSession").addEventListener("click", createSessionHandler);
	document.getElementById("launchtest").addEventListener("click", launchTestHandler);
	
	
	//Launch Test
	function launchTestHandler() {
	  		var options = {
			"launchType": "message"
		};
		var showToolbar = document.getElementById("toolbar").checked;
		options["preferences"]={
									"ui": {
										"toolbar": {
													"menubar": showToolbar
												},
												
												"sessionsize" :{
													"minwidth" : 200,
													"minheight" : 200
									}
								}
		};
		citrix.receiver.createSession(citrixReceiverId, options, responseCallback);
		function responseCallback(response) {
			sessionObj = response;
		}
	  
	  
	  if (sessionObj) {
			var launchData = {};
			//Place data here for managed app
			var icaData = {
        "Domain":loginDomain,
        "Password":"",
        "AutologonAllowed":"ON",
        "BrowserProtocol":"HTTPonTCP",
        "ClearPassword":userPassword,
        "InitialProgram":icaInitialProgram,
        "Title":icaTitle,
        "Address":icaAddress,
        //"InitialProgram":"#Internet Explorer",
        //"Title":"IE",
        //"Address":"35.233.182.68",
        "Username":userUsername
    };
    launchData = {"type" :"json",value :icaData};
			sessionObj.start(launchData, successCallback);
			function successCallback(response) {
				console.log('start ', response);
			}
			sessionObj.addListener('onConnection', listenerCallback);
			sessionObj.addListener('onConnectionClosed', onConnectionClosedCallback);
			sessionObj.addListener('onError', listenerCallback);
			sessionObj.addListener('onURLRedirection', listenerCallback);
		}
		
		
	}
	//END LAUNCH TEST
	
	function createSessionHandler() {
		var options = {
			"launchType": "message"
		};
		var showToolbar = document.getElementById("toolbar").checked;
		options["preferences"]={
									"ui": {
										"toolbar": {
													"menubar": showToolbar
												},
												
												"sessionsize" :{
													"minwidth" : 200,
													"minheight" : 200
									}
								}
		};
		citrix.receiver.createSession(citrixReceiverId, options, responseCallback);
		function responseCallback(response) {
			sessionObj = response;
		}
	}
			
	function onConnectionClosedCallback() {
		if (response.type == "onConnectionClosed") {
			console.log('onConnectionClosed event triggered', response);
			sessionObj = null;
		}
	}
	
	function listenerCallback(response) {
		if (response.type == "onConnection") {
			console.log('onConnection event triggered', response);
		} else if (response.type == "onError") {
			console.log('onError event triggered', response);
		} else if (response.type == "onURLRedirection") {
			console.log('onURLRedirection event triggered', response);
		}
	}
}
